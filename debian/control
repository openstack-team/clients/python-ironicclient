Source: python-ironicclient
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Corey Bryant <corey.bryant@canonical.com>,
 David Della Vecchia <ddv@canonical.com>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr (>= 6.0.0),
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-cliff,
 python3-ddt,
 python3-dogpile.cache,
 python3-fixtures,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-openstackclient,
 python3-openstackdocstheme,
 python3-openstacksdk,
 python3-osc-lib,
 python3-oslo.utils,
 python3-oslotest,
 python3-platformdirs,
 python3-requests,
 python3-requests-mock,
 python3-sphinxcontrib.apidoc,
 python3-stestr,
 python3-stevedore,
 python3-subunit,
 python3-tempest,
 python3-testtools,
 python3-yaml,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/clients/python-ironicclient
Vcs-Git: https://salsa.debian.org/openstack-team/clients/python-ironicclient.git
Homepage: http://www.openstack.org

Package: python-ironicclient-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Client for OpenStack bare metal Service - doc
 Ironic provision bare metal machines instead of virtual machines. It is a fork
 of the Nova Baremetal driver. It is best thought of as a bare metal hypervisor
 API and a set of plugins which interact with the bare metal hypervisors. By
 default, it will use PXE and IPMI in concert to provision and turn on/off
 machines, but Ironic also supports vendor-specific plugins which may
 implement
 additional functionality.
 .
 This is a client for the OpenStack Ironic API. There's a Python API
 (the "ironicclient" module), and a command-line script ("ironic").
 .
 Installing this package gets you a shell command, that you can use to
 interact with Ironic's API.
 .
 This package provides the documentation.

Package: python3-ironicclient
Architecture: all
Depends:
 python3-cliff,
 python3-dogpile.cache,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-openstackclient,
 python3-openstacksdk,
 python3-osc-lib,
 python3-oslo.utils,
 python3-pbr (>= 6.0.0),
 python3-platformdirs,
 python3-requests,
 python3-stevedore,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Description: Client for OpenStack bare metal Service - Python 3.x
 Ironic provision bare metal machines instead of virtual machines. It is a fork
 of the Nova Baremetal driver. It is best thought of as a bare metal hypervisor
 API and a set of plugins which interact with the bare metal hypervisors. By
 default, it will use PXE and IPMI in concert to provision and turn on/off
 machines, but Ironic also supports vendor-specific plugins which may
 implement
 additional functionality.
 .
 This is a client for the OpenStack Ironic API. There's a Python API
 (the "ironicclient" module), and a command-line script ("ironic").
 .
 Installing this package gets you a shell command, that you can use to
 interact with Ironic's API.
 .
 This package provides the Python 3.x support.
